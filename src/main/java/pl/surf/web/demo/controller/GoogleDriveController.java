package pl.surf.web.demo.controller;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.surf.web.demo.google.service.GoogleDriveServiceImp;
import pl.surf.web.demo.model.requests.FileRequest;
import pl.surf.web.demo.model.responses.Image;

import java.io.*;

@RestController
@RequestMapping("api/upload")
public class GoogleDriveController {

    private GoogleDriveServiceImp googleDriveService;
    private Image image;

    public GoogleDriveController(GoogleDriveServiceImp googleDriveService, Image image) {
        this.googleDriveService = googleDriveService;
        this.image = image;
    }

    //    @PostMapping()
//    public Image uploadImageToDrive(@RequestParam("file") MultipartFile multipartFile) throws IOException {
//        File convFile = new File(multipartFile.getOriginalFilename());
//        convFile.createNewFile();
//        FileOutputStream fos = new FileOutputStream(convFile);
//        System.out.println(fos);
//        System.out.println(convFile);
//        fos.write(multipartFile.getBytes());
//        fos.close();
//        com.google.api.services.drive.model.File file2 = googleDriveService.uoLoadFile(convFile.getName(), convFile.getAbsolutePath(), "image/jpg");
//        image.setId(file2.getId());
//        image.setWebContentLink(file2.getWebContentLink());
//        image.setWebViewLink(file2.getWebViewLink());
//        return image;
//    }
    @PostMapping()
    public Image uploadImageToDrive(@RequestBody String imageString) throws IOException {

        File convFile = new File("nameOfFile");
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(imageString.getBytes());
        fos.close();
        com.google.api.services.drive.model.File file2 = googleDriveService.uoLoadFile(convFile.getName(), convFile.getAbsolutePath(), "image/jpg");
        this.image.setId(file2.getId());
        this.image.setWebContentLink(file2.getWebContentLink());
        this.image.setWebViewLink(file2.getWebViewLink());
        return this.image;
    }
}
