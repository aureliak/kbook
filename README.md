Projekt wykonywany w ramach Hackatyk 2020


## Mapa funkcjonalności  

<img src="https://i.imgur.com/UuNTxDL.png" style="zoom:40%;" />

# Stack

- MySQL
- Spring Boot
- Hibernate
- JWT
- JUnit
